============
Installation
============

At the command line::

    $ pip install os-namos

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv os-namos
    $ pip install os-namos
