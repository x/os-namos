===============================
os-namos
===============================

Discovery agent of namos service.


* Free software: Apache license
* Documentation: http://docs.openstack.org/developer/os-namos
* Source: http://git.openstack.org/cgit/openstack/os-namos
* Bugs: http://bugs.launchpad.net/soman

Features
--------

* Register the given OpenStack service component under namos and acknowledge it
* Helps to find the live status of OpenStack service components
* Helps to update the configuration files lively.

How to setup os-namos
---------------------
* Assume, os-namos is cloned at /opt/stack/os-namos, then run below command to
  install namos from this directory.

  `sudo python setup.py install`

How to use this agent
---------------------

* Invoke this agent from the service component console python scripts, where
  its launched.
